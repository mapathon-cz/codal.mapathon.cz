#!/bin/bash
if [ $# -ne 1 ]
then
        echo 'Translate to all languages.'
        echo ''
        echo 'Usage:'
        echo -e '\tbash translate_all.sh DIRECTORY'
        exit 1
fi

dest=$1
mkdir $dest
for folder in `ls lang`
do
        mkdir ./$dest/$folder
        cp index.html ./$dest/$folder/
        cp info.js ./$dest/$folder/
        for file in `ls ./lang/$folder`
        do
                cp ./$file.gv ./$dest/$folder/$file.gv
                while read -r line
                do
                        sed -i "s|\<${line/=/\\>|}|" ./$dest/$folder/$file.gv
                done < ./lang/$folder/$file
                dot ./$dest/$folder/$file.gv -Tsvg > ./$dest/$folder/$file.svg
        done
done
