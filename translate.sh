#!/bin/bash
if [ $# -ne 1 ]
then
        echo 'Translate graphs.'
        echo ''
        echo 'Translated strings must be in folder `LANG`.'
        echo ''
        echo 'Usage:'
        echo -e '\tbash translate.sh LANG'
        exit 1
fi

folder=$1
for file in `ls $folder`
do
        while read -r line
        do
                sed -i "s|\<${line/=/\\>|}|" ./$file.gv
        done < ./$folder/$file
done
