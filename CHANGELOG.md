# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased]

## [0.3.0] - 2019-06-13
### Added
- Multilingual support.

## [0.2.1] - 2019-06-11
### Fixed
- Missing `info.js` file in Gitlab CI/CD.

## [0.2.0] - 2019-06-11
### Added
- Show node's info.

## 0.1.0 - 2019-06-09
### Added
- Changelog, license, readme.
- Graph of roles at mapathon.
- Legend to graph.
- `index.html` file with both images.

[Unreleased]: https://gitlab.com/qeef/codal.mapathon.cz/compare/v0.3.0...master
[0.3.0]: https://gitlab.com/qeef/codal.mapathon.cz/compare/v0.2.1...v0.3.0
[0.2.1]: https://gitlab.com/qeef/codal.mapathon.cz/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/qeef/codal.mapathon.cz/compare/v0.1.0...v0.2.0
